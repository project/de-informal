de-informal
===========

Diese Übersetzungsdatei enthält *ausschließlich* Zeichenketten, die sich von
der Hauptübersetzung (http://drupal.org/project/de) unterscheiden. Importiere
diese Übersetzung mit der Option "überschreiben", damit die Zeichenketten, die
formelle Anreden enthalten mit den Informellen ("Du") überschrieben werden.